Key,File,Type,UsedInMainMenu,NoTranslate,english,Context / Alternate Text,german,latam,french,italian,japanese,koreana,polish,brazilian,russian,turkish,schinese,tchinese,spanish

PlusXP,items,Item,,,Пилюли для мозга
PlusXPDesc,items,Item,,,Использование этого предмета предназначено в особых случаях для крайней нужды получения опыта!

foodBelish,items,Item,,,Бэлиш
foodBelishDesc,items,Item,,,Бэлиш

foodHoneyComb,items,Item,,,Медовые соты
foodHoneyCombDesc,items,Item,,,"Можно насобирать несколько штук и приготовить банку мёда, можно съесть и так вместе с воском но врядли твой желудок это оценит!"

foodDonut,items,Item,,,Sergiy’s donut
foodDonutDesc,items,Item,,,"Эти красивые и аппетитные Пончики Уникальное блюдо приготовленное по особому секретному рецепту SirJulik!"

foodShamPie,items,Item,,,Пирог со Срамом
foodShamPieDesc,items,Item,,,Пирог со Срамом

foodDog1,items,Item,,,Томатный Дог
foodDog1Desc,items,Item,,,Томатный Дог

foodDog2,items,Item,,,Дог с Ветчиной
foodDog2Desc,items,Item,,,Дог с Ветчиной

foodDog3,items,Item,,,Дог с Курицей и Грушей
foodDog3Desc,items,Item,,,Дог с Курицей и Грушей

foodChakchak,items,Item,,,Чакчак
foodChakchakDesc,items,Item,,,Чакчак

foodKoimak,items,Item,,,Коймак
foodKoimakDesc,items,Item,,,Коймак

foodSnowberryPuding,items,Item,,,Морозный пудинг
foodSnowberryPudingDesc,items,Item,,,Морозный пудинг

foodEchpochmak,items,Item,,,Эчпочмак
foodEchpochmakDesc,items,Item,,,Эчпочмак

foodKastibii,items,Item,,,Кыстыбый
foodKastibiiDesc,items,Item,,,Кыстыбый

Sugar,items,Item,,,Сахар
SugarDesc,items,Item,,,Сахар

Sir,items,Item,,,Заплесневелый сыр
SirDesc,items,Item,,,Заплесневелый сыр

Samogon,items,Item,,,Самогон
SamogonDesc,items,Item,,,Самогон

Mead,items,Item,,,Медовуха
MeadDesc,items,Item,,,Медовуха

foodTesto,items,Item,,,Тесто
foodTestoDesc,items,Item,,,Тесто

foodPizzaFirmen,items,Item,,,Леле пицца Фирменная
foodPizzaFirmenDesc,items,Item,,,Леле пицца Фирменная

foodPizzaTropic,items,Item,,,Леле пицца Тропическая
foodPizzaTropicDesc,items,Item,,,Леле пицца Тропическая

foodPizzaFire,items,Item,,,Леле пицца с Огоньком
foodPizzaFireDesc,items,Item,,,Леле пицца с Огоньком

foodPizzaStrange,items,Item,,,Подозрительная пицца
foodPizzaStrangeDesc,items,Item,,,Подозрительная пицца или Стрэндж пицца. Неважно как она называется. Важно то из чего она состоит!

foodPizzaMargarita,items,Item,,,Пицца Маргарита
foodPizzaMargaritaDesc,items,Item,,,Пицца Маргарита

foodCheburek,items,Item,,,Чебурек
foodCheburekDesc,items,Item,,,Чебурек

CanAnanas,items,Item,,,Консервированный ананас
CanAnanasDesc,items,Item,,,Консервированный ананас, способен вывести радиацию.

CanTomato,items,Item,,,Консервированный томат
CanTomatoDesc,items,Item,,,Консервированный томат

FizzyBubblech,items,Item,,,Сладкий Бубалех
FizzyBubblechDesc,items,Item,,,Невероятно тонизирующий и освежающий напиток!

transfusionpack,items,Item,,,Пакет для переливаний
transfusionpackDesc,items,Item,,,"С помощью пакета для прелеваний можно собрать собственную кровь, для изготовления аптечки!"

foodPelmeni,items,Item,,,Пельмени
foodPelmeniDesc,items,Item,,,"Просто,вкусно, а главное питательно!"

DryRations,items,Item,,,ИРП
DryRationsDesc,items,Item,,,"Индивидуальный рацион питания, сухо́й паёк — набор продуктов, предназначенный для питания военнослужащих, а также гражданских лиц в условиях отсутствия возможности готовить горячую пищу."