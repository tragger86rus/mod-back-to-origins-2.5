

The following assets are used in this mod:

	Name mod "YR Menu Music" - Author: "Yakov"
	Name mod "FeralSensePlus" - Author: "Games'N'Grumle"
	Name mod "zz_Trader Network_MoreJobs" - Author: "FuriousRamsay"
	Name mod "Lootable Corps Mod" - Author: "Survager"
	Name mod "LowWattagePosts" - Author: "Genosis"	
	Name mod "Bdubs Vehicles" - Author: "bdubyah"		
	Name mod "Marauder" - Author: "bdubyah"	
	Name mod "OcbPinRecipes/OcbPinRecipesUiTdrHart" - Author: "ocbMaurice"		
	Name mod "MD-500" - Author: "bdubyah"		
	Name mod "Semi" - Author: "bdubyah"	
	Name mod "Duster" - Author: "bdubyah"		
	Name mod "UAZ 452" - Author: "bdubyah"		
	Name mod "Buggy" - Author: "bdubyah"		
	Name mod "Dirt Bike" - Author: "bdubyah"			
    Name mod "TFP Behemoths" - Author: "KhaineGB"
    Name mod "TFP Hornet" - Author: "KhaineGB"	
	Name mod "Vanilia Weapon Sound Expansion" - Author: "TheReggie"
	Name mod "FirearmsExpansion4" - Author: "Smadol" https://www.nexusmods.com/7daystodie/mods/1335
	Name mod "Service lift" - Author: "HammerJade"
	Name mod "Syrinuv JetArmor mod" - Author: "Syrinuv"
	Name mod "ZZTong Prefabs" - Author: "ZZTong"	
	Name mod "ZMXuiCPBBMxl" - Author: "Sirillion"	https://www.nexusmods.com/7daystodie/mods/22
	Name mod "ZMXuiCPTFS" - Author: "Sirillion"	https://www.nexusmods.com/7daystodie/mods/22
	Name mod "ZMXuiCP" - Author: "Sirillion"	https://www.nexusmods.com/7daystodie/mods/22
	Name mod "ZMXhudCPTHB" - Author: "Sirillion"	https://www.nexusmods.com/7daystodie/mods/22
	Name mod "ZMXhudCPBH1080" - Author: "Sirillion"	https://www.nexusmods.com/7daystodie/mods/22
	Name mod "ZMXhudCP" - Author: "Sirillion"	https://www.nexusmods.com/7daystodie/mods/22
	Name mod "SMXui" - Author: "Sirillion"	https://www.nexusmods.com/7daystodie/mods/22
	Name mod "SMXlib" - Author: "Sirillion"	https://www.nexusmods.com/7daystodie/mods/22
	Name mod "SMXhud" - Author: "Sirillion"	https://www.nexusmods.com/7daystodie/mods/22
	Name mod "Pick up wild plants mod, by Khaine" - Author: "KhaineGB"	
	Name mod "SMXmenu" - Author: "Sirillion"	https://www.nexusmods.com/7daystodie/mods/22
	Name mod "13erserk - Traps" - Author: "13erserk"				
	Name mod "Extended Game Options" - Author: "Deadlee6"		
	Name mod "0-SCore" - Author: "sphereii"
	Name mod "Vanilla AK-47 Replacer" - Author: "Sykriss"
	Name mod "AC's Vehicle Modlet" - Author: "ACTINIUMTIGER,GuppyCur,Ragsy"
	Name mod "JaxTeller718-ActionSkills" - Author: "JaxTeller718"
	Name mod "Telrics Flamethrower A20" - Author: "Telric"
	Name mod "Locks" - Author: "Xyth/SphereII/Mumpfy"
	Name mod "Browning M1919A4" - Author: "Coaber"
	Name mod "GodSponge Chef Fix" - Author: "GodSponge"
	Name mod "HuntingRifle_Old" - Author: "Deceptive Pastry"	
	Name mod "A19 Farming For A20" - Author: "Syco54645"		
	Name mod "Tactical Weapons" - Author: "ViperInfinity, Tristam, IvanXD"		
	Name mod "Telrics Melee Weapons Pack A19" - Author: "Telric"		
	Name mod "EnhancedCraftAndRepair" - Author: "Dizor"	Website "https://github.com/IDizor/7D2D-EnhancedCraftAndRepair" />
	Name mod "Vehicle Damage Patch" - Author: "KhaineGB"
	Name mod "UnlockOpenDoors" - Author: "Dizor" Website "https://github.com/IDizor/7D2D-UnlockOpenDoors" />
	Name mod "OcbWayPointIcons" - Author: "ocbMaurice" Website "https://github.com/OCB7D2D/OcbWayPointIcons" />
	Name mod "QuestAreaTimer" - Author: "Dizor" Website "https://github.com/IDizor/7D2D-QuestAreaTimer" />
	Name mod "GK Old System Upgrade" - Author: "Gouki"	
	Name mod "ShowRemainingToClear" - Author: "aedenthorn"	
	
Thanks to the authors of mods for their kind provision!!!
